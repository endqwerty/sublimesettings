Start with Sublime Text closed and Package Manager installed

1. Preferences > Browse Packages
2. Delete User folder
3. Clone Repo and rename to user folder
4. Open Sublime Text 3
5. Wait for it to break itself while installing packages.

Documentation provided at: https://packagecontrol.io/docs/syncing